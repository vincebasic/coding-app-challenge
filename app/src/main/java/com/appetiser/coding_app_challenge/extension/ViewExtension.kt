package com.appetiser.coding_app_challenge.extension

import android.view.View
import android.widget.ImageView
import com.appetiser.coding_app_challenge.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar

fun ImageView.loadItemImage(uri: String?){
    val options = RequestOptions()
            .dontAnimate()
    Glide.with(this.context)
            .setDefaultRequestOptions(options)
            .load(uri)
            .dontAnimate()
            .fitCenter()
            .centerCrop()
            .into(this)
}

fun showSnackBar(
        parentView: View,
        text: String
): Snackbar {
    return Snackbar.make(parentView, text, Snackbar.LENGTH_LONG)
            .apply {
                setTextColor(
                        parentView.context.getColor(R.color.white)
                )
                setBackgroundTint(
                        parentView.context.getColor(R.color.purple_700)
                )
                show()
            }
}