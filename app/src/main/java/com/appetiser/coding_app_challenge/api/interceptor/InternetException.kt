package com.appetiser.coding_app_challenge.api.interceptor

import java.lang.Exception

class InternetException (message: String) : Exception(message)