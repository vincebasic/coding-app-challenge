package com.appetiser.coding_app_challenge.api

import com.appetiser.coding_app_challenge.models.TrackResult
import io.reactivex.Observable
import retrofit2.http.GET

interface TrackHttp {
    @GET("search?term=star&amp;country=au&amp;media=movie&amp;all")
    fun getTracks() : Observable<TrackResult>
}