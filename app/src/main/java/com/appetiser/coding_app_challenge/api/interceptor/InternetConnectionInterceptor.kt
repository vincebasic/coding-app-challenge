package com.appetiser.coding_app_challenge.api.interceptor

import com.appetiser.coding_app_challenge.CodingApp
import com.appetiser.coding_app_challenge.R
import okhttp3.Interceptor
import okhttp3.Response

class InternetConnectionInterceptor(var app: CodingApp) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if(!app.isAppOnline()){
            app.showMessage(R.string.internet_unavailable)
            throw InternetException(app.getString(R.string.internet_unavailable))
        }
        return chain.proceed(chain.request())
    }
}