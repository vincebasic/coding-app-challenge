package com.appetiser.coding_app_challenge.di.module

import com.appetiser.coding_app_challenge.ViewModelFactory
import com.appetiser.coding_app_challenge.api.TrackHttp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelModule {
    @Provides
    @Singleton
    fun providesTrackListViewModelFactory(track: TrackHttp): ViewModelFactory {
        return ViewModelFactory(track)
    }
}