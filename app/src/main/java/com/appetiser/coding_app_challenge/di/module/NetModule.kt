package com.appetiser.coding_app_challenge.di.module

import com.appetiser.coding_app_challenge.CodingApp
import com.appetiser.coding_app_challenge.api.TrackHttp
import com.appetiser.coding_app_challenge.api.errorhandler.RxErrorHandlingCallAdapterFactory
import com.appetiser.coding_app_challenge.api.interceptor.InternetConnectionInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module class NetModule {

    companion object {
        const val API_NEW : String = "API_NEW"
    }

    @Provides
    @Singleton
    fun provideInternetInterceptor(app:CodingApp) = InternetConnectionInterceptor(app)

    @Provides
    @Singleton
    fun providesOkHttpClient(internetConnectionInterceptor: InternetConnectionInterceptor) : OkHttpClient {
        val interceptor =  HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(internetConnectionInterceptor)
            .hostnameVerifier { _, _ -> true }
            .build()
    }

    @Provides @Singleton @Named(API_NEW)
    fun providesRetrofit(client: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://itunes.apple.com/")
            .client(client)
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides @Singleton
    fun providesTrackHttp(@Named(API_NEW) retrofit: Retrofit) : TrackHttp = retrofit.create(TrackHttp::class.java)
}