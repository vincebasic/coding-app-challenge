package com.appetiser.coding_app_challenge.di.scope

import javax.inject.Scope

@Scope annotation class ActivityScope
