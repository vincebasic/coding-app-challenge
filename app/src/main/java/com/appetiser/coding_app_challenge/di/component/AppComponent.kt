package com.appetiser.coding_app_challenge.di.component

import android.app.Application
import com.appetiser.coding_app_challenge.CodingApp
import com.appetiser.coding_app_challenge.di.builder.ActivityBuilder
import com.appetiser.coding_app_challenge.di.builder.FragmentBuilder
import dagger.Component
import dagger.android.AndroidInjectionModule
import com.appetiser.coding_app_challenge.di.module.AppModule
import com.appetiser.coding_app_challenge.di.module.NetModule
import com.appetiser.coding_app_challenge.di.module.ViewModelModule
import dagger.BindsInstance
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        NetModule::class,
        ViewModelModule::class,
        ActivityBuilder::class,
        FragmentBuilder::class
    ]
)

interface AppComponent {
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application) : Builder

        fun build() : AppComponent
    }

    fun inject(app : CodingApp)
}