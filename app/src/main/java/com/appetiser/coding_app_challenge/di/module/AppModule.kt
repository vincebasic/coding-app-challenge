package com.appetiser.coding_app_challenge.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.appetiser.coding_app_challenge.CodingApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module class AppModule {

    @Provides
    @Singleton
    fun providesCodingApplication(app: Application) : CodingApp = app as CodingApp

    @Provides
    @Singleton
    fun providesContext(app: CodingApp) : Context = app.applicationContext

    @Provides
    @Singleton
    fun providesPreferences(context: Context) : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
}
