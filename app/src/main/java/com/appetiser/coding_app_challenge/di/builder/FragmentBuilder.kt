package com.appetiser.coding_app_challenge.di.builder

import com.appetiser.coding_app_challenge.di.scope.FragmentScope
import com.appetiser.coding_app_challenge.ui.track.TrackListFragment
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun provideTrackListFragment(): TrackListFragment
}