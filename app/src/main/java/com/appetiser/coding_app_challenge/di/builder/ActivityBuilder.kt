package com.appetiser.coding_app_challenge.di.builder

import com.appetiser.coding_app_challenge.di.scope.ActivityScope
import com.appetiser.coding_app_challenge.ui.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}