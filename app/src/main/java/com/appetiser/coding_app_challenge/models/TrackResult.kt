package com.appetiser.coding_app_challenge.models

data class TrackResult (
    var resultCount : Int? = null,
    var results: List<Track>? = null
)