package com.appetiser.coding_app_challenge.ui.track

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.appetiser.coding_app_challenge.api.TrackHttp
import com.appetiser.coding_app_challenge.models.TrackResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TrackListViewModel @Inject constructor(private val trackHttp: TrackHttp) : ViewModel() {

    private val disposables: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    private var _trackResult = MutableLiveData<TrackResult>()
    var trackResult : LiveData<TrackResult> = _trackResult

    fun getTracks(){
        trackHttp.getTracks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    Log.d("MainVM", "Subscribing...")
                }
                .subscribe ({
                    _trackResult.value = it
                }, {
                    it.printStackTrace()
                })
            .addTo(disposables)
    }
}