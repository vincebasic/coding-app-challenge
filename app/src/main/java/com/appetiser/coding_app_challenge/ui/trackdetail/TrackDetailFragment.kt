package com.appetiser.coding_app_challenge.ui.trackdetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.appetiser.coding_app_challenge.databinding.FragmentTrackDetailBinding
import com.appetiser.coding_app_challenge.extension.loadItemImage


class TrackDetailFragment : Fragment() {

    private lateinit var binding: FragmentTrackDetailBinding
    private val args: TrackDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTrackDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with (binding){

            imgTrackDetail.loadItemImage(args.track.artworkUrl100)

            txtTrackNameDetail.text = args.track.getValidTrackName()

            txtTrackGenreDetail.text = args.track.primaryGenreName

            btnTrackPriceDetail.text = args.track.getTrackPrice()

            txtTrackDescDetail.text = args.track.getValidDescription()

            imgBack.setOnClickListener { activity?.onBackPressed() }
        }
    }
}