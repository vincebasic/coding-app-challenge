package com.appetiser.coding_app_challenge.ui.track.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.appetiser.coding_app_challenge.databinding.AdapterTrackBinding
import com.appetiser.coding_app_challenge.extension.loadItemImage
import com.appetiser.coding_app_challenge.models.Track

class TrackAdapter (private val trackList: List<Track>, var onTrackClickedListener: OnTrackClickListener) : androidx.recyclerview.widget.RecyclerView.Adapter<TrackAdapter.ViewHolder>() {

    interface OnTrackClickListener{
        fun onTrackClicked(track: Track)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = AdapterTrackBinding.inflate(layoutInflater, parent, false)

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return trackList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val icon = trackList[position]
        holder.bindItemIcon(icon)
    }

    inner class ViewHolder(val binding: AdapterTrackBinding): androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bindItemIcon(track: Track){
            with(binding) {

                txtTrackName.text = track.getValidTrackName()

                txtTrackGenre.text = track.primaryGenreName

                txtTrackPrice.text = track.getTrackPrice()

                if(!track.artworkUrl100.isNullOrEmpty())imgTrack.loadItemImage(track.artworkUrl100)

                binding.viewTrackItem.setOnClickListener { onTrackClickedListener.onTrackClicked(track) }
            }
        }
    }
}