package com.appetiser.coding_app_challenge.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.edit
import com.appetiser.coding_app_challenge.CodingApp
import com.appetiser.coding_app_challenge.databinding.ActivityMainBinding
import com.appetiser.coding_app_challenge.extension.getCurrentDateTime
import com.appetiser.coding_app_challenge.extension.showSnackBar
import com.appetiser.coding_app_challenge.extension.toString

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val currentDate = getCurrentDateTime()
    private val dateInString = currentDate.toString("dd/MMMM/yyyy")
    private val getFormattedDate = CodingApp.sharedPreferences?.getString("date", "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        CodingApp.sharedPreferences?.edit {
            putString("date", dateInString)
        }
        if(!getFormattedDate.isNullOrEmpty()){
            showSnackBar(binding.root, "Previously visited date: $getFormattedDate")
        }
    }
}
/**
 * Persistence
 * The persistence in the app that I created is that every time the user will launch the app,
 * A date when the user previously visited will be saved on shared preference,
 * that will be displayed on a SnackBar.
 * **/

/**
 * Architecture
 * MVVM (Model View View Model)
 * It provides better separation of UI and Logic
 * The reason why I used the MVVM architecture is because currently we are working on rebuilding our projects using MVVM
 * I am more familiar with MVP but I want to learn more about MVVM and be comfortable with it
 * **/