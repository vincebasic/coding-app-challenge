package com.appetiser.coding_app_challenge.ui.track

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.appetiser.coding_app_challenge.ViewModelFactory
import com.appetiser.coding_app_challenge.api.TrackHttp
import com.appetiser.coding_app_challenge.databinding.FragmentTrackListBinding
import com.appetiser.coding_app_challenge.models.Track
import com.appetiser.coding_app_challenge.ui.track.adapter.TrackAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class TrackListFragment : Fragment(), TrackAdapter.OnTrackClickListener {

    private lateinit var binding: FragmentTrackListBinding

    @Inject
    lateinit var api: TrackHttp

    lateinit var factory: ViewModelFactory

    lateinit var viewModel: TrackListViewModel

    var trackAdapter : TrackAdapter? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        factory = ViewModelFactory(api)
        viewModel = ViewModelProvider(this, factory).get(TrackListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTrackListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getTracks()
        setupObserver()

    }

    private fun setupObserver(){
        viewModel.trackResult.observe(viewLifecycleOwner, {
            it.results?.let { tracks ->
                setupRvTrack(tracks)
            }
        })
    }

    private fun setupRvTrack(trackList: List<Track>){
        trackAdapter = TrackAdapter(trackList, this)
        binding.rvTracks.adapter = trackAdapter
    }

    override fun onTrackClicked(track: Track) {
        findNavController().navigate(
            TrackListFragmentDirections.actionTrackListFragmentToTrackDetailFragment(track)
        )
    }
}